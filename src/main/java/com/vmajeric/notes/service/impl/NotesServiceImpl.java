package com.vmajeric.notes.service.impl;

import com.vmajeric.notes.model.Notes;
import com.vmajeric.notes.service.NotesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class NotesServiceImpl implements NotesService {

	@Value("${vmajeric.app.path_to_notes}")
	private String path;

	private List<String> readLines() {
		List<String> lines = new ArrayList<>();
		try (BufferedReader in = new BufferedReader(new FileReader(this.path))) {
			String line;
			while ((line = in.readLine()) != null)
				lines.add(line);
		} catch (IOException ex) {
			System.err.println(ex);
		}

		return lines;
	}

	public Notes getNotes() {

		StringBuilder sb = new StringBuilder();
		this.readLines().forEach(l -> sb.append(l).append(System.lineSeparator()));
		return new Notes(sb.toString());

	}

	public String getNotesAsHTML() {
		StringBuilder sb = new StringBuilder();
		this.readLines().forEach(l -> sb.append(l).append("<br>"));
		return sb.toString();
	}

	public void writeNotes(String notes) {
		try (BufferedWriter out = new BufferedWriter(new FileWriter(path, false))){
			out.append(notes);
			out.flush();
		}
		catch (IOException ex) {
			System.err.println(ex);
		}
	}
}
