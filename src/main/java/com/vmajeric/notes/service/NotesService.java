package com.vmajeric.notes.service;

import org.springframework.stereotype.Service;

import com.vmajeric.notes.model.Notes;

@Service
public interface NotesService {
	Notes getNotes();
	String getNotesAsHTML();
	void writeNotes(String notes);
}
