package com.vmajeric.notes.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Value("${vmajeric.app.adminUsername}")
    private String adminUsername;
    @Value("${vmajeric.app.userUsername}")
    private String userUsername;
    @Value("${vmajeric.app.adminPassword}")
    private String adminPassword;
    @Value("${vmajeric.app.userPassword}")
    private String userPassword;

    private final PasswordEncoder passwordEncoder;

    public UserDetailsServiceImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        username=username.trim();

        if(username.equals(adminUsername))
            return User.withUsername(adminUsername).password(passwordEncoder.encode(adminPassword)).roles("ADMIN", "USER").build();

        if(username.equals(userUsername))
            return User.withUsername(userUsername).password(passwordEncoder.encode(userPassword)).roles("USER").build();

        throw new UsernameNotFoundException("No such user");


    }
}
