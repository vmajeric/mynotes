package com.vmajeric.notes.model;

public class Notes {

	private String notes;

	public Notes(String notes) {
		super();
		this.notes = notes;
	}
	
	public Notes() {
		super();
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public String toString() {
		return this.notes;
	}
	
	
}
