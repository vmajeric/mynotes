package com.vmajeric.notes.controller;

import com.vmajeric.notes.model.Notes;
import com.vmajeric.notes.service.NotesService;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/notes")
@RestController
public class NotesController {

	private final NotesService notesService;

	public NotesController(NotesService notesService) {
		this.notesService = notesService;
	}

	@GetMapping("/string")
	public Notes getNotes() {
		return new Notes(notesService.getNotes().toString());
	}
	
	@GetMapping("/text")
	public String getText() {
		return notesService.getNotesAsHTML();
	}
	
	@PostMapping("/text")
	public void writeText(@RequestBody String text) {
		notesService.writeNotes(text);
	}
}
